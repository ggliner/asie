var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var winston = require('winston');	

// var configureRoutes = require('./routes.js');

 var logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)()
    ]
  });

var mongodbRest = require('./mongodb-rest/server.js');
mongodbRest.startServer(logger);	

var app = express();


var MongoClient = require('mongodb').MongoClient
// Connection URL
var url = 'mongodb://localhost:27017/db';
// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {

  app.use(morgan('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));


  // configureRoutes(app, db);


  app.listen(3005, function() {
    console.log('Listening');
  });
  console.log("Connected correctly to server");
  db.close();
});
